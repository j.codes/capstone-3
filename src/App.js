import './App.css';
import {useState} from 'react'
import {Container} from 'react-bootstrap'
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom'
import {UserProvider} from './UserContext'

import AppNavbar from './components/AppNavbar'
import Home from './pages/Home'
import Login from './pages/Login'
import Register from './pages/Register'
import Logout from './pages/Logout'
import Catalog from './pages/Catalog'
import ProductView from './components/ProductView'
import Dashboard from './pages/Dashboard'
import Orders from './pages/Orders'
import Users from './pages/Users'
import Cart from './pages/Cart'
import ErrorPage from './pages/ErrorPage'



function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  return (
    <>
      <UserProvider value={{user, setUser, unsetUser}}> 
        <Router>
          <AppNavbar/>
          <Container>
            <Routes>
              <Route path="/" element={<Home/>}/>
              <Route path="/products" element={<Catalog/>}/>
              <Route path="/products/:productId/view" element={<ProductView/>}/>
              <Route path="/login" element={<Login/>}/>
              <Route path="/register" element={<Register/>}/>
              <Route path="/dashboard" element={<Dashboard/>}/>
              <Route path="/orders" element={<Orders/>}/>
              <Route path="/users" element={<Users/>}/>
              <Route path="/cart" element={<Cart/>}/>
              <Route path="/logout" element={<Logout/>}/>
              <Route path="*" element={<ErrorPage/>}/>
            </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>
  );
}

export default App;
