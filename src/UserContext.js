import React from 'react'

// Initializes a react context
const UserContext = React.createContext()

// Initializes a context provider
// Gives us ability to provide a specific context through a component
export const UserProvider = UserContext.Provider // exports UserProvider with the UserContext

export default UserContext //export default - exports the whole file