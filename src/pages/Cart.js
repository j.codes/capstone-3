import  React, {useState, useEffect, useContext} from 'react'
import {Card, Button, Modal, Table} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import UserContext from '../UserContext'
import CartView from '../components/CartView'
import Loading from '../components/Loading'
import Swal from 'sweetalert2'

export default function Cart(){
  const {user} = useContext(UserContext)  

  const [cart, setCart] = useState([])

  const [itemTotal, setItemTotal] = useState(0)
  const [orderID, setOrderID] = useState()

  const [isLoading, setIsLoading] = useState(false)

  function orderCheckout(event){
    event.preventDefault()

    fetch(`${process.env.REACT_APP_API_URL}/order/${orderID}/checkout`, {
      method: 'PATCH',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(result => {
      if(result){
        Swal.fire({
          title: "Success",
          icon: "success",
          text: "Order check-out successful!"
        })
      } else {
        Swal.fire({
          title: "Oops!",
          icon: "error",
          text: "Something went wrong!"
        })
      }
    })
  }

  useEffect(() => {
    setIsLoading(true)

    fetch(`${process.env.REACT_APP_API_URL}/order/active`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(response => response.json())
    .then(result => {
      if(result[0]){
      setOrderID(result[0]._id)

      setCart(
        result[0].products.map((product, index) => {
          return (
            <CartView key={product._id} orderId={result[0]._id} product={product} index={index} passChildData={setItemTotal} total={result[0].totalAmount}/>
          )
        })
      )
    }
      setIsLoading(false)
    })
  }, [])

  return(

    (isLoading) ?
      <Loading/>
    :
      <>
        <Table striped className="cartTable mt-2 text-center">
          <thead>
            <tr className="cartTableHead">
              <th>Product Name</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Subtotal</th>
              <th>Action</th>
            </tr>
          </thead>    
          <tbody>
            {cart}
          </tbody>
          <tfoot>
            <tr className="cartTableHead">
              <th colSpan={3}><Button variant="primary" className="" onClick={event => orderCheckout(event)}>Checkout</Button></th>
              <th className="cartTotal" colSpan={2}>Total: Php {itemTotal}</th>
            </tr>
          </tfoot>
        </Table>                              
      </>
  )
}