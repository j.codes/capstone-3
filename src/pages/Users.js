import  React, {useState, useEffect, useContext} from 'react'
import {Card, Button, Modal, Table} from 'react-bootstrap'
import {Link, Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Loading from '../components/Loading'
import UserView from '../components/UserView'

export default function Users(){
  const {user} = useContext(UserContext)  

  const [isLoading, setIsLoading] = useState(false)

  const [allUsers, setAllUsers] = useState([])

  useEffect(() => {
    setIsLoading(true)

    fetch(`${process.env.REACT_APP_API_URL}/users/all`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(response => response.json())
    .then(result => {
      setAllUsers(
        result.map((user) => {
          return (
            <UserView key={user._id} user={user}/>
          )
        })
      )
      setIsLoading(false)
    })
  }, [])

  return(
    (user.id && user.isAdmin) ?
      (isLoading) ?
        <Loading/>
      :
        <>
          <div className="text-center mt-3">
            <h1 className="bannerHeader">Registered Users</h1>
          </div>

          <Table striped className="text-center mt-4">
            <thead>
              <tr className="cartTableHead">
                <th>ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Contact Number</th>
                <th>Access Level</th>
                <th colSpan={2}>Actions</th>
              </tr>
            </thead>    
            <tbody>
              {allUsers}
            </tbody>
          </Table>                         
        </>
    : 
      <Navigate to="/"/>
  )
}