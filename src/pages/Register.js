import	{useState, useEffect, useContext} from 'react'
import{Form, Button} from 'react-bootstrap'
import {useNavigate, Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Register() {
	const {user} = useContext(UserContext) 

	const [email, setEmail] = useState('')
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')
	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [mobileNo, setMobileNo] = useState('')

	const navigate = useNavigate()

	const [isActive, setIsActive] = useState(false)

	function registerUser(event){
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/check-email`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(response => response.json())
		.then(result => {
			if(result === true){
				Swal.fire({
					title: 'Oops!',
					icon: 'error',
					text: 'Email already exists!'
				})
			} else {
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: 'POST',
					headers: {
						"Content-Type": "application/json",
						Authorization: `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1
					})
				})
				.then(response => response.json())
				.then(result => {
					if(result){
						// Clears out the input fields after form submission
						setEmail('')
						setPassword1('')
						setPassword2('')
						setFirstName('')
						setLastName('')
						setMobileNo('')

						Swal.fire({
							title: "Success",
							icon: "success",
							text: "Register successful!"
						})

						navigate('/login')
					} else {
						Swal.fire({
							title: "Oops!",
							icon: "error",
							text: "Something went wrong!"
						})
					}
				})
			}
		})
	}

	useEffect(() => {
		if((firstName !== '' && lastName !== '' && mobileNo.length === 11 && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			// Enables the submit button if the form data has been verified
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [firstName, lastName, mobileNo, email, password1, password2])

	return(
		(user.id !== null) ?
			<Navigate to="/courses"/>
		:
			<div className="d-flex justify-content-center mt-2">
				<Form className="registerForm p-5" onSubmit={event => registerUser(event)}>
					<Form.Group controlId="firstName">
					    <Form.Label className="formLable">First Name</Form.Label>
					    <Form.Control 
					        type="text" 
					        placeholder="Enter first name" 
					        value={firstName}
					        onChange={event => setFirstName(event.target.value)}
					        required
					    />
					</Form.Group>
					<Form.Group controlId="lastName">
					    <Form.Label className="formLable pt-1">Last Name</Form.Label>
					    <Form.Control 
					        type="text" 
					        placeholder="Enter last name" 
					        value={lastName}
					        onChange={event => setLastName(event.target.value)}
					        required
					    />
					</Form.Group>
					<Form.Group controlId="mobileNo">
					    <Form.Label className="formLable pt-1">Mobile Number</Form.Label>
					    <Form.Control 
					        type="text" 
					        placeholder="Enter mobile number" 
					        value={mobileNo}
					        onChange={event => setMobileNo(event.target.value)}
					        required
					    />
					</Form.Group>
			        <Form.Group controlId="userEmail">
			            <Form.Label className="formLable pt-1">Email address</Form.Label>
			            <Form.Control 
			                type="email" 
			                placeholder="Enter email" 
			                value={email}
			                onChange={event => setEmail(event.target.value)}
			                required
			            />
			        </Form.Group>

			        <Form.Group controlId="password1">
			            <Form.Label className="formLable pt-1">Password</Form.Label>
			            <Form.Control 
			                type="password" 
			                placeholder="Password" 
			                value={password1}
			                onChange={event => setPassword1(event.target.value)}
			                required
			            />
			        </Form.Group>

			        <Form.Group controlId="password2">
			            <Form.Label className="formLable pt-1">Verify Password</Form.Label>
			            <Form.Control 
			                type="password" 
			                placeholder="Verify Password" 
			                value={password2}
			                onChange={event => setPassword2(event.target.value)}
			                required
			                className="mb-3" //added code only
			            />
			        </Form.Group>

			        {
			        	isActive ?
			        	<Button variant="primary" type="submit" id="submitBtn">
			        		Register
			        	</Button>
			        	:
			        	<Button variant="primary" type="submit" id="submitBtn" disabled>
			        		Register
			        	</Button>
			        }
			        
			    </Form>
		    </div>
	)
}