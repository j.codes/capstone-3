import  React, {useState, useEffect, useContext} from 'react'
import {Card, Button, Modal, Table} from 'react-bootstrap'
import {Link, Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Products from '../components/Products'
import Loading from '../components/Loading'
import UpdateProduct from '../components/UpdateProduct'
import AddProduct from '../components/AddProduct'

export default function Dashboard(){
  const {user} = useContext(UserContext)  

  const [catalog, setCatalog] = useState([])

  const [isLoading, setIsLoading] = useState(false)

  const[refresh, setRefresh] = useState(true)
  const[addRefresh, setAddRefresh] = useState(false)

  useEffect(() => {
    setIsLoading(true)

    fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(response => response.json())
    .then(result => {
      setCatalog(
        result.map((product) => {
          return (
            <Products key={product._id} product={product} passChildData={setRefresh}/>
          )
        })
      )
      setIsLoading(false)
    })
  }, [refresh, addRefresh])

  return(
    (user.id && user.isAdmin) ?
      (isLoading) ?
        <Loading/>
      :
        <>
          <div className="text-center mt-3">
            <h1 className="bannerHeader">Admin Dashboard</h1>
            <AddProduct productId="" product="" passChildData={setAddRefresh}/>
          </div>

          <Table striped className="text-center mt-4">
            <thead>
              <tr className="cartTableHead">
                <th>ID</th>
                <th>Product Name</th>
                <th>Description</th>
                <th>Price</th>
                <th>Stocks</th>
                <th>Availability</th>
                <th>Category</th>
                <th colSpan={2}>Actions</th>
              </tr>
            </thead>    
            <tbody>
              {catalog}
            </tbody>
          </Table>                         
        </>
    : 
      <Navigate to="/"/>
  )
}