import Products from '../components/Products'
import Loading from '../components/Loading'
import	{useEffect, useState} from 'react'

export default function Catalog(){
	const [catalog, setCatalog] = useState([])
	const [isLoading, setIsLoading] = useState(false)

	const[refresh, setRefresh] = useState(true)
	
	useEffect(() => {
		setIsLoading(true)

		fetch(`${process.env.REACT_APP_API_URL}/products/active`)
		.then(response => response.json())
	    .then(result => {
	      setCatalog(
	        result.map((product) => {
	          return (
	            <Products key={product._id} product={product} passChildData={setRefresh}/>
	          )
	        })
	      )
	      setIsLoading(false)
	    })
	}, [])

	return(	
		(isLoading) ?
			<Loading/>
		:
			<>
				<div className="d-flex flex-wrap flex-row p-2 text-center">
					{catalog}
				</div>
			</>
	)
}