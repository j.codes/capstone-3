import {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import {useNavigate, Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Login() {
	const {user, setUser} = useContext(UserContext)

	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')

	const navigate = useNavigate()

	const [isActive, setIsActive] = useState(false)

	const retrieveUser = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(response => response.json())
		.then(result => {

			setUser({
				id: result._id,
				isAdmin: result.isAdmin
			})
		})
	}

	function authenticate(event) {
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(response => response.json())
		.then(result => {
			if(typeof result.accessToken !== "undefined"){
				localStorage.setItem('token', result.accessToken)

				retrieveUser(result.accessToken)

				Swal.fire({
					title: 'Login Successfull',
					icon: 'success',
					text: 'Welcome to BMTea!'
				})
			} else {
				Swal.fire({
					title: 'Authentication Failed!',
					icon: 'error',
					text: 'Incorrect email or password'
				})
			}
		})
	}

	useEffect(() => {
		if(email !== '' && password !== ''){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password])

	return(
		(user.id !== null) ?
			(user.isAdmin === false) ?
				<Navigate to="/"/>
			:
				<Navigate to="/dashboard"/> // /dashboard
		:
			<div className="d-flex justify-content-center mt-5">
				<Form className="loginForm p-5" onSubmit={event => authenticate(event)}>
			        <Form.Group controlId="userEmail">
			            <Form.Label className="formLable">Email address</Form.Label>
			            <Form.Control 
			                type="email" 
			                placeholder="Enter email" 
			                value={email}
			                onChange={event => setEmail(event.target.value)}
			                required
			            />
			        </Form.Group>

			        <Form.Group controlId="password" className="mt-3 mb-3">
			            <Form.Label className="formLable">Password</Form.Label>
			            <Form.Control 
			                type="password" 
			                placeholder="Password" 
			                value={password}
			                onChange={event => setPassword(event.target.value)}
			                required
			            />
			        </Form.Group>

			        {
			        	isActive ?
			        	<Button variant="primary" type="submit" id="submitBtn">
			        		Login
			        	</Button>
			        	:
			        	<Button variant="primary" type="submit" id="submitBtn" disabled>
			        		Login
			        	</Button>
			        }
			    </Form>
		    </div>
	)
}