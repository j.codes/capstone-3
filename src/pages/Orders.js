import  React, {useState, useEffect, useContext} from 'react'
import {Card, Button, Modal, Table} from 'react-bootstrap'
import {Link, Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Loading from '../components/Loading'
import OrderView from '../components/OrderView'

export default function Orders(){
  const {user} = useContext(UserContext)  

  const [orderedProducts, setOrderedProducts] = useState([])

  const [isLoading, setIsLoading] = useState(false)

  useEffect(() => {
    setIsLoading(true)

    if(user.id && user.isAdmin){
      fetch(`${process.env.REACT_APP_API_URL}/order/all`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(response => response.json())
      .then(result => {
        setOrderedProducts(
          result.map((order) => {
            return (
              <OrderView key={order._id} order={order}/>
            )
          })
        )
        setIsLoading(false)
      })
    } else {
      fetch(`${process.env.REACT_APP_API_URL}/order/history`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(response => response.json())
      .then(result => {
        setOrderedProducts(
          result.map((order) => {
            return (
              <OrderView key={order._id} order={order}/>
            )
          })
        )
        setIsLoading(false)
      })
    } 
  }, [])

  return(
    (user.id && user.isAdmin) ?
      (isLoading) ?
        <Loading/>
      :
        <>
          <div className="text-center mt-3">
            <h1 className="bannerHeader">All Orders</h1>
          </div>

          <Table striped className="mt-4">
            <thead>
              <tr className="cartTableHead">
                <th>User ID</th>
                <th>Order ID</th>
                <th>Products</th>
                <th>Date of Purchase</th>
                <th>Total Amount</th>
                <th>Order Status</th>
              </tr>
            </thead>    
            <tbody>
              {orderedProducts}
            </tbody>
          </Table>                         
        </>
    : 
      (isLoading) ?
        <Loading/>
      :
        <>
          <div className="text-center mt-3">
            <h1 className="bannerHeader">Order History</h1>
          </div>

          <Table striped className="mt-4">
            <thead>
              <tr className="cartTableHead">
                <th>Order ID</th>
                <th>Products</th>
                <th>Date of Purchase</th>
                <th>Total Amount</th>
                <th>Order Status</th>
              </tr>
            </thead>    
            <tbody>
              {orderedProducts}
            </tbody>
          </Table>                         
        </>
  )
}