import React, {useState, useEffect, useContext} from 'react'
import {Card, Button, Table, Modal} from 'react-bootstrap'
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom'
import UserContext from '../UserContext'
import UpdateProduct from '../components/UpdateProduct'
import Swal from 'sweetalert2'

import teaPicture from "../assets/sample-tea.jpg"

export default function Products(product){
	const {user} = useContext(UserContext)  

	const {_id, productName, description, price, stocks, isActive, category} = product.product

	const [productAvailability, setProductAvailability] = useState(isActive)

	const[refresh, setRefresh] = useState(false)

	function disableProduct(event){
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/archive`, {
		  method: 'PATCH',
		  headers: {
		    "Content-Type": "application/json",
		    Authorization: `Bearer ${localStorage.getItem('token')}`
		  },
		  body: JSON.stringify({
		    isActive: false
		  })
		})
		.then(response => response.json())
		.then(result => {
		  if(result){
		    Swal.fire({
		      title: "Success",
		      icon: "success",
		      text: "Update product availability successful!"
		    })

		    setProductAvailability(false)
		  } else {
		    Swal.fire({
		      title: "Oops!",
		      icon: "error",
		      text: "Something went wrong!"
		    })
		  }
		})
	}

	function enableProduct(event){
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/archive`, {
		  method: 'PATCH',
		  headers: {
		    "Content-Type": "application/json",
		    Authorization: `Bearer ${localStorage.getItem('token')}`
		  },
		  body: JSON.stringify({
		    isActive: true
		  })
		})
		.then(response => response.json())
		.then(result => {
		  if(result){
		    Swal.fire({
		      title: "Success",
		      icon: "success",
		      text: "Update product availability successful!"
		    })

		    setProductAvailability(true)
		  } else {
		    Swal.fire({
		      title: "Oops!",
		      icon: "error",
		      text: "Something went wrong!"
		    })
		  }
		})
	}

	useEffect(() => {
		product.passChildData(refresh)
	}, [refresh])

	return(
		(user.isAdmin === false || user.isAdmin === null) ?
			<div className="p-2">
				<Card className="productCard h-100 mb-3 p-2">
				  <Card.Img variant="top" src={teaPicture}/>
			      <Card.Body>
			        <Card.Title className="productCardTitle py-1">{productName}</Card.Title>
			        <Card.Text>{description}<br/></Card.Text>
			        <Card.Text>Price: {price}</Card.Text>
			        <Link className="btn btn-primary" to={`/products/${_id}/view`}>Details</Link>
			      </Card.Body>
			    </Card>
		    </div>
		:
			<React.Fragment key={product._id}>
			  <tr>
			   <td className = "productId">{_id}</td>
			   <td className = "productName">{productName}</td> 
			   <td className = "productDescription">{description}</td>
			   <td className = "productPrice">{price}</td>
			   <td className = "productStocks">{stocks}</td>
			   {(productAvailability) ?
			   	<td className = "productIsActive">Available</td>
			   :
			   	<td className = "productIsActive">Not Available</td>
			   }
			   <td className = "productCategory">{category}</td>
			   <td className = "editProduct"><UpdateProduct productId={_id} product={product.product} passChildData={setRefresh}/></td>
			   {(productAvailability) ?
			   	<td className = "disableProduct"><Button variant="danger" onClick={event => disableProduct(event)}>Disable</Button></td>
			   : 
			   	<td className = "enableProduct"><Button variant="primary" onClick={event => enableProduct(event)}>Enable</Button></td>
			   }
			 </tr>
			</React.Fragment>
	)
}

Products.propTypes = {
	product: PropTypes.shape({
		productName: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}