import React, {useState, useEffect, useContext} from 'react'
import {Button} from 'react-bootstrap'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function UserView({user}){
	const {_id, firstName, lastName, email, mobileNo, isAdmin} = user

	const [userAdmin, setUserAdmin] = useState(isAdmin)

	function setAsRegular(event){
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/${_id}/admin`, {
		  method: 'PATCH',
		  headers: {
		    "Content-Type": "application/json",
		    Authorization: `Bearer ${localStorage.getItem('token')}`
		  },
		  body: JSON.stringify({
		    isAdmin: false
		  })
		})
		.then(response => response.json())
		.then(result => {
		  if(result){
		    Swal.fire({
		      title: "Success",
		      icon: "success",
		      text: "Update user access level successful!"
		    })

		    setUserAdmin(false)
		  } else {
		    Swal.fire({
		      title: "Oops!",
		      icon: "error",
		      text: "Something went wrong!"
		    })
		  }
		})
	}

	function setAsAdmin(event){
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/${_id}/admin`, {
		  method: 'PATCH',
		  headers: {
		    "Content-Type": "application/json",
		    Authorization: `Bearer ${localStorage.getItem('token')}`
		  },
		  body: JSON.stringify({
		    isAdmin: true
		  })
		})
		.then(response => response.json())
		.then(result => {
		  if(result){
		    Swal.fire({
		      title: "Success",
		      icon: "success",
		      text: "Update user access level successful!"
		    })

		    setUserAdmin(true)
		  } else {
		    Swal.fire({
		      title: "Oops!",
		      icon: "error",
		      text: "Something went wrong!"
		    })
		  }
		})
	}

	return(
		<React.Fragment key={user._id}>
		  <tr>
		   <td className = "userId">{_id}</td>
		   <td className = "firstName">{firstName}</td> 
		   <td className = "lastName">{lastName}</td>
		   <td className = "email">{email}</td>
		   <td className = "mobileNo">{mobileNo}</td>
		   {(userAdmin) ?
		   	<td className = "userAdmin">User</td>
		   :
		   	<td className = "userAdmin">Admin</td>
		   }
		   {(userAdmin) ?
		   	<td className = "setAsRegular"><Button variant="danger" onClick={event => setAsRegular(event)}>Set As Regular</Button></td>
		   : 
		   	<td className = "setAsAdmin"><Button variant="primary" onClick={event => setAsAdmin(event)}>Set As Admin</Button></td>
		   }
		 </tr>
		</React.Fragment>
	)
}