import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function OrderedProductView({product}) {
	const {user} = useContext(UserContext)
	const navigate = useNavigate()

	const {productId, quantity, subTotal} = product

	const [productName, setProductName] = useState();
	const [price, setPrice] = useState();

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/view`)
		.then(response => response.json())
		.then(result => {
			setProductName(result.productName)
			setPrice(result.price)
		})
	}, [productId])

	return(
		<li>{quantity}x {productName} at {price} each. Subtotal: {subTotal}</li>
	)
}