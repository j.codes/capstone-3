import React, {useState, useEffect, useContext} from 'react'
import {Card, Button, Table, Modal} from 'react-bootstrap'
import UserContext from '../UserContext'
import OrderedProductsView from '../components/OrderedProductsView'

export default function OrderView({order}){
	const {user} = useContext(UserContext)  

	const {_id, userId, totalAmount, purchasedOn, orderStatus, products} = order

	const [orderedProducts, setOrderedProducts] = useState();

	useEffect(() => {
		setOrderedProducts(
		  products.map((product) => {
		    return (
		      <OrderedProductsView key={product.productId} product={product}/>
		    )
		  })
		)
	}, [_id])

	return(
		(user.isAdmin === false || user.isAdmin === null) ?
			<React.Fragment key={order._id}>
			  <tr>
			   <td className = "orderId">{_id}</td>
			   <td className = "products">
			   		<ol>
			   			{orderedProducts}
			   		</ol>
			   </td>
			   <td className = "purchasedOn">{purchasedOn}</td>
			   <td className = "totalAmount">{totalAmount}</td>
			   <td className = "orderStatus">{orderStatus}</td>
			 </tr>
			</React.Fragment>
		:
			<React.Fragment key={order._id}>
			  <tr>
			   <td className = "orderId">{_id}</td>
			   <td className = "userId">{userId}</td> 
			   <td className = "products">
			   		<ol>
			   			{orderedProducts}
			   		</ol>
			   </td>
			   <td className = "purchasedOn">{purchasedOn}</td>
			   <td className = "totalAmount">{totalAmount}</td>
			   <td className = "orderStatus">{orderStatus}</td>
			 </tr>
			</React.Fragment>
	)
}