import {Button, Row, Col, Card} from 'react-bootstrap'
import {Link} from 'react-router-dom'

import teaBundles from "../assets/tea-bundles.jpg"
import teaAccesories from "../assets/tea-accesories.jpg"
import teaCategories from "../assets/tea-categories.jpg"

export default function Banner() {
	return(
		<>
			<Row>
				<Col className="bannerContents pt-5">
					<div>
						<h1 className="bannerHeader text-center">Welcome to the Baguio Mountain Tea Shop</h1>
						<p className="bannerText text-center">Baguio Mountain Tea Shop is a unique Loose leaf specialist Tea Merchant that stocks and retails different types of loose leaf teas and blends. All teas are premium grade, pesticide free, and includes a large range that are Certified Organic and non-GMO.</p>
						<div className="text-center">
							<Link className="btn btn-primary" to={`/products`}>Shop Now!</Link>
						</div>
					</div>
				</Col>
			</Row>
			<Row>
				<Col className="bannerCardContents pt-5 d-flex justify-content-center">
					<div className="bannerCard1 p-2">
						<Card className="bannerCard bg-dark text-white">
					      <Card.Img className="bannerImage" src={teaBundles} alt="Card image" />
					      <Card.ImgOverlay>
					        <Card.Title className="bannerCardTitle">Tea Bundles</Card.Title>
					        <Card.Text>
					        </Card.Text>
					      </Card.ImgOverlay>
					    </Card>
					</div>
					<div className="bannerCard1 p-2">
						<Card className="bannerCard bg-dark text-white">
					      <Card.Img className="bannerImage" src={teaAccesories} alt="Card image" />
					      <Card.ImgOverlay>
					        <Card.Title className="bannerCardTitle">Tea Accesories</Card.Title>
					        <Card.Text>
					        </Card.Text>
					      </Card.ImgOverlay>
					    </Card>
					</div>
					<div className="bannerCard1 p-2">
						<Card className="bannerCard bg-dark text-white">
					      <Card.Img className="bannerImage" src={teaCategories} alt="Card image" />
					      <Card.ImgOverlay>
					        <Card.Title className="bannerCardTitle">Tea Categories</Card.Title>
					        <Card.Text>
					        </Card.Text>
					      </Card.ImgOverlay>
					    </Card>
					</div>
				</Col>
			</Row>
		</>
	)
}