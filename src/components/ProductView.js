import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function ProductView() {

	const {productId} = useParams()

	const {user} = useContext(UserContext)
	const navigate = useNavigate()

	const [productName, setProductName] = useState("product name");
	const [description, setDescription] = useState("product description");
	const [price, setPrice] = useState(0);
	const [stocks, setStocks] = useState(0);

	const [addcart,setaddcart]=useState(1);

	const AddCart = () => {
		if(addcart < stocks){ 
			setaddcart(addcart + 1) 
		} 
	} 

	const DecBag = () => {
    	if(addcart > 1){
    		setaddcart(addcart - 1)
    	}
	}

	const addToCart = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/cart/add`, {
			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				userId: user.id,
				quantity: addcart
			})
		})
		.then(response => response.json())
		.then(result => {
			if(result){
				Swal.fire({
					title: "Success",
					icon: "success",
					text: "You have enrolled successfully!"
				})

				navigate('/products')
			} else {
				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
					text: "Please try again :("
				})
			}
		})
	}

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/view`)
		.then(response => response.json())
		.then(result => {
			setProductName(result.productName)
			setDescription(result.description)
			setPrice(result.price)
			setStocks(result.stocks)
		})
	}, [productId])

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title className="productCardTitle">{productName}</Card.Title>
							<Card.Text>{description}</Card.Text>
							<Card.Text>Price: PhP {price}</Card.Text>

							<div className="qty d-flex justify-content-center">
							    <i className="px-2" onClick={DecBag}> - </i>
							    <p>{addcart}</p>
							    <i className="px-2" onClick={AddCart}> + </i>
							</div>

							{
								user.id !== null ?
									<Button variant="primary" onClick={() => addToCart(productId)} /*hidden={user.id === null}*/>Add to Cart</Button>
								:

									<Link className="btn btn-danger btn-block" to="/login">log in to buy</Link>
							}
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
	)
}