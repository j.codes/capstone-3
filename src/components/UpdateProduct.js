import  {useState, useEffect, useContext} from 'react'
import {Modal, Button, Form} from 'react-bootstrap'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function UpdateProduct(data){
  const {user} = useContext(UserContext) 

  const {_id, productName, description, price, stocks, isActive, category} = data.product

  const [items, setItems] = useState([]) 
  const [show, setShow] = useState(false)
  const [isLoading, setIsLoading] = useState(false)


  const handleClose = () => setShow(false)
  
  const handleShow = () => setShow(true)

  const [itemName, setItemName] = useState(productName)
  const [itemDescription, setItemDescription] = useState(description)
  const [itemPrice, setItemPrice] = useState(price)
  const [itemStocks, setItemStocks] = useState(stocks)
  const [itemCategory, setItemCategory] = useState(category)

  function updateProduct(event){
    fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/update`, {
      method: 'PATCH',
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        name: itemName,
        description: itemDescription,
        price: itemPrice,
        stocks: itemStocks,
        category: itemCategory
      })
    })
    .then(response => response.json())
    .then(result => {
      if(result){
        Swal.fire({
          title: "Success",
          icon: "success",
          text: "Update product successful!"
        })

        data.passChildData(true)
        setShow(false)
      } else {
        Swal.fire({
          title: "Oops!",
          icon: "error",
          text: "Something went wrong!"
        })
      }
    })
  }

  useEffect(() => {
    setIsLoading(true)

    if(show){
      setItems(
        <>
           <Form.Group className="mb-3" controlId="productName">
             <Form.Label>Product Name:</Form.Label>
             <Form.Control
               type="text"
               value = {itemName}
               onChange={event => setItemName(event.target.value)}
               required
             />
           </Form.Group>
           <Form.Group
              className="mb-3"
              controlId="productDescription"
            >
              <Form.Label>Description</Form.Label>
              <Form.Control as="textarea" rows={3}
              value = {itemDescription} 
              onChange={event => setItemDescription(event.target.value)}
              required
              />
            </Form.Group>
           <Form.Group className="mb-3" controlId="productPrice">
             <Form.Label>Price:</Form.Label>
             <Form.Control
               type="number"
               value = {itemPrice}
               onChange={event => setItemPrice(event.target.value)}
               required
             />
           </Form.Group>
           <Form.Group className="mb-3" controlId="productStocks">
             <Form.Label>Stocks:</Form.Label>
             <Form.Control
               type="number"
               value = {itemStocks}
               onChange={event => setItemStocks(event.target.value)}
               required
             />
           </Form.Group>
           <Form.Group className="mb-3" controlId="productCategory">
             <Form.Label>Tea Category:</Form.Label>
             <select aria-label="Default select example" className="form-select" defaultValue={itemCategory} onChange={event => setItemCategory(event.target.value)}>
             <option value="Flower Tea">Flower Tea</option>
             <option value="Herbal Tea">Herbal Tea</option>
             <option value="Green Tea">Green Tea</option>
             <option value="Black Tea">Black Tea</option>
             <option value="Oolong Tea">Oolong Tea</option>
             </select>
           </Form.Group>
        </>
      )
    }
    setIsLoading(false)
  }, [show, itemName, itemDescription, itemPrice, itemStocks, itemCategory])

  return (
    <>
      <Button className="btn btn-primary" onClick={handleShow}>
        Edit
      </Button>

      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>Update A Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            {items}
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={event => updateProduct(event)}>Update</Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}