import React, { useState, useEffect, useContext, useRef } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

let tempFinalItemTotal = 0
let finalItemTotal = 0

export default function CartView(data){
	const {_id, productId, quantity, subTotal} = data.product

	const [productName, setProductName] = useState("product name");
	const [price, setPrice] = useState(0);
	const [stocks, setStocks] = useState(0);

	const [itemSubTotal, setItemSubTotal] = useState(subTotal);
	const [itemTotal, setItemTotal] = useState(data.total);
	const previousItemTotal = useRef();

	const [index, setIndex] = useState(data.index + 1);
	const [itemQuantity,setItemQuantity]=useState(quantity);

	const handleDelete = () => {
		setItemQuantity(0)
		setItemTotal(itemTotal - itemSubTotal)
		tempFinalItemTotal -= itemSubTotal
		setItemSubTotal(0) 
	}

	const AddCart = () => {
		if(itemQuantity < stocks){ 
			setItemQuantity(itemQuantity + 1) 
			setItemSubTotal(price * (itemQuantity + 1))
			tempFinalItemTotal += price
			editCart(itemQuantity + 1)
		} 
	} 

	const DecBag = () => {
    	if(itemQuantity > 1){
    		setItemQuantity(itemQuantity - 1)
    		setItemSubTotal(price * (itemQuantity - 1))
    		tempFinalItemTotal -= price
    		editCart(itemQuantity - 1)
    	}
	}

	function handleDeleteProduct(event){
		event.preventDefault()

		alert(`Do you want to remove ${productName} from your cart?`)

		handleDelete()

		editCart(0)
	}

	function editCart(itemQty){
		fetch(`${process.env.REACT_APP_API_URL}/cart/${data.orderId}/edit`, {
		  method: 'PATCH',
		  headers: {
		    "Content-Type": "application/json",
		    Authorization: `Bearer ${localStorage.getItem('token')}`
		  },
		  body: JSON.stringify({
		    productId: productId,
		    quantity: itemQty
		  })
		})
		.then(response => response.json())
		.then(result => {
			if(itemQty == 0){
			  if(result){
			    Swal.fire({
			      title: "Success",
			      icon: "success",
			      text: "Successfully removed the product from your cart!"
			    })
			  } else {
			    Swal.fire({
			      title: "Oops!",
			      icon: "error",
			      text: "Something went wrong!"
			    })
			  }
			}
		})
	}

	useEffect(() => {
		finalItemTotal = data.total  + tempFinalItemTotal
		data.passChildData(finalItemTotal)
	}, [itemTotal, itemQuantity])

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/view`)
		.then(response => response.json())
		.then(result => {
			setProductName(result.productName)
			setPrice(result.price)
			setStocks(result.stocks)
		})
	}, [productId])

	return(
		<React.Fragment key={data._id}>
		  <tr>
		   <td className = "productName">{productName}</td>
		   <td className = "productName">{price}</td>
		   <td className = "productQuantity">
			<div className="qty d-flex justify-content-center">
			    <i className="px-2" onClick={DecBag}> - </i>
			    <p>{itemQuantity}</p>
			    <i className="px-2" onClick={AddCart}> + </i>
			</div>
		   </td> 
		   <td className = "productSubTotal">{itemSubTotal}</td>
		   <td className = "disableProduct"><Button variant="danger" onClick={event => handleDeleteProduct(event)}>Remove</Button></td>
		 </tr>
		</React.Fragment>
	)
}