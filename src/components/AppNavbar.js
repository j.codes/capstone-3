import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Offcanvas from 'react-bootstrap/Offcanvas';
import {Link, NavLink} from 'react-router-dom'
import {useState, useEffect, useContext} from 'react'
import UserContext from '../UserContext'

function AppNavbar() {
	const {user} = useContext(UserContext)

	const expand = 'sm'

	return (
	<>
	  {
	    <Navbar id="navbar" key={expand} expand={expand}>
	      <Container fluid>
	        <Navbar.Brand className="navBrand px-5" href="#">Baguio Mountain Tea</Navbar.Brand>
	        <Navbar.Toggle aria-controls={`offcanvasNavbar-expand-${expand}`} />
	        <Navbar.Offcanvas
	          id={`offcanvasNavbar-expand-${expand}`}
	          aria-labelledby={`offcanvasNavbarLabel-expand-${expand}`}
	          placement="end"
	          className="px-5"
	        >
	          <Offcanvas.Header closeButton>
	            <Offcanvas.Title id={`offcanvasNavbarLabel-expand-${expand}`}>
	              Baguio Mountain Tea
	            </Offcanvas.Title>
	          </Offcanvas.Header>
	          <Offcanvas.Body>
	            <Nav className="justify-content-end flex-grow-1 pe-3">
	              <Nav.Link as={NavLink} to="/">Home</Nav.Link>
	              {
	              	
	              	(user.isAdmin) ?
	              	  <>
		              	  <Nav.Link as={NavLink} to="/dashboard">Dashboard</Nav.Link>
		              	  <Nav.Link as={NavLink} to="/orders">Orders</Nav.Link>
		              	  <Nav.Link as={NavLink} to="/users">Users</Nav.Link>
	              	  </>
	              	:
	              	  <>
		              <Nav.Link as={NavLink} to="/products">Products</Nav.Link>
		              {
		              (user.id) ?
		              	<>
			              	<Nav.Link as={NavLink} to="/cart">Cart</Nav.Link>
			              	<Nav.Link as={NavLink} to="/orders">Orders</Nav.Link>
		              	</>
		              :
		              	<Nav.Link></Nav.Link>
		          	  }
	              	  </>
	              }
	              <NavDropdown
	                title="Menu"
	                id={`offcanvasNavbarDropdown-expand-${expand}`}
	              >	
	              	{
	              		(user.id) ?

	              			<NavDropdown.Item as={NavLink} to="/logout">Logout</NavDropdown.Item>
	              		:
	              			<>
		              		<NavDropdown.Item as={NavLink} to="/login">Login</NavDropdown.Item>
		              		<NavDropdown.Item as={NavLink} to="/register">Register</NavDropdown.Item>
		              		</>
	              	}
	                
	              </NavDropdown>
	            </Nav>
	          </Offcanvas.Body>
	        </Navbar.Offcanvas>
	      </Container>
	    </Navbar>
	  }
	</>
	);
}

export default AppNavbar