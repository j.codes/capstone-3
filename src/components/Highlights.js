import Carousel from 'react-bootstrap/Carousel'

import flowerTea from "../assets/flower-tea.jpg"
import greenTea from "../assets/green-tea.jpg"
import herbalTea from "../assets/herbal-tea.jpg"
import blackTea from "../assets/black-tea.jpg"
import oolongTea from "../assets/oolong-tea.jpg"

export default function Highlights() {
  return (
    <Carousel id="highlights">
      <Carousel.Item interval={1500}>
        <img
          className="highlightsPicture d-block w-100"
          src={flowerTea}
          alt="First slide"
        />
        <Carousel.Caption>
          <h3 className="highlightsHeader">Flower Tea</h3>
          <p className="highlightsText">You can now give the tea lovers in your life gorgeous flowers, that taste great! Our floral teas include jasmine, lavender, hibiscus, rosebuds, roses, and chamomile. Our blends span a wide variety, including herbal, green, white, and black teas. Each is adorned with fresh dried flowers to soothe the senses in aroma, in flavor, and visually. You'll take one sip, and slip into bliss.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item interval={1500}>
        <img
          className="highlightsPicture d-block w-100"
          src={herbalTea}
          alt="Second slide"
        />
        <Carousel.Caption>
          <h3 className="highlightsHeader">Herbal Tea</h3>
          <p className="highlightsText">Our selection of herbal teas includes a wide array of naturally caffeine-free teas, also called tisanes, including rooibos, mint, chamomile, hibiscus, rose tea and a variety of herbal tea blends. Many herbal teas are known for having medicinal qualities, such as calming, throat soothing, and sleepy teas. Check out the specific health benefits of each tea listed in the "benefits" tab of each product page. These 100% naturally caffeine-free healthy beverages can be sipped all throughout the day for a healthy boost without any jitters. We also feature our new decaf green tea blend here, for those who want a traditional green tea that's 99% caffeine-free.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item interval={1500}>
        <img
          className="highlightsPicture d-block w-100"
          src={greenTea}
          alt="Third slide"
        />
        <Carousel.Caption>
          <h3 className="highlightsHeader">Green Tea</h3>
          <p className="highlightsText">
            Our loose green tea selection offers a variety of signature blends and single-origin green teas in bulk. Medical research suggests that green tea can decrease your risk of certain cancers and aid in weight loss by stimulating fat-burning mechanisms. The antioxidants in green tea, primarily EGCG polyphenols, are thought to be responsible for the unique health benefits of green tea. Fresh loose leaf green tea is an enjoyable part of a healthy lifestyle. Our premium green teas are available in bulk, tins, tea samplers, as well as a quarterly Green Tea Club.
          </p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item interval={1500}>
        <img
          className="highlightsPicture d-block w-100"
          src={blackTea}
          alt="Third slide"
        />
        <Carousel.Caption>
          <h3 className="highlightsHeader">Black Tea</h3>
          <p className="highlightsText">
            Our selection of loose black teas include traditional blends, such as English Breakfast and Russian Caravan, unique twists on Earl Grey, as well as single-origin black leaf teas such as assam, ceylon, and lapsang souchong. Medical research shows that black tea may help lower cholesterol, decrease risks of heart disease and hypertension, and has anti-bacterial properties. This selection of loose black tea is available in bulk, sachets, tea samplers, as well as a quarterly Black Tea Club.
          </p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item interval={1500}>
        <img
          className="highlightsPicture d-block w-100"
          src={oolongTea}
          alt="Third slide"
        />
        <Carousel.Caption>
          <h3 className="highlightsHeader">Oolong Tea</h3>
          <p className="highlightsText">
            Oolongs are mid-range in oxidation, between green and black teas. Our loose leaf oolong selection contains premium Chinese oolongs, including a traditional tie guan yin and milk oolong blend. Medical research suggests that oolong aids in weight loss through fat burning mechanisms, beyond that of green tea. Wu long tea, well known for weight loss, is simply a different way of spelling oolong in english. Our whole leaf oolongs are available in bulk and sachets.
          </p>
        </Carousel.Caption>
      </Carousel.Item>

    </Carousel>
  );
}